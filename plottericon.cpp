#include "plottericon.h"
#include "plotterconfig.h"

#include <QPainter>
#include <QDebug>
#include <cmath>

PlotterIcon::PlotterIcon(PlotterConfig *c, QObject * parent) : QSystemTrayIcon(parent)
{
	log = NULL;
	config = c;
	cursor = 0;
	setLogSize(22);
	redraw();
}

int PlotterIcon::getLogSize() const
{
	return logSize;
}

void PlotterIcon::setLogSize(int value)
{
	logSize = value;
	if (log != NULL) delete log;
	log = new quint64[logSize];
	memset(log, 0, logSize * sizeof(quint64));
}

void PlotterIcon::inputData(quint64 value)
{
	log[cursor] = value;
	cursor += 1;
	if(cursor >= getLogSize())
		cursor -= getLogSize();
}

void PlotterIcon::redraw()
{
	QPixmap pm(22,22);
	pm.fill(config->getBackgroundColor());
	QPainter painter(&pm);

	// Grid
	painter.setPen(config->getGridColor());
	for (int i = 0; i < 22; i+= config->getGridDensity())
		painter.drawLine(0, 22 - i, 21, 22 - i);

	// Graph
	painter.setPen(config->getColor());
	int startPosition = cursor + 1;
	if (startPosition >= getLogSize())
		startPosition -= getLogSize();
	int i = startPosition;
	int graphPosition = 0;
	do
	{
		int barWidth = round((double)log[i] / (double)config->getScale() * 21 + 1);

		painter.drawLine(graphPosition, 22, graphPosition, 22 - barWidth);
		graphPosition++;
		i++;
		if (i >= getLogSize())
			i -= getLogSize();
	} while(i != startPosition);

	// Label
	painter.setPen(config->getTextColor());
	painter.setFont(QFont(config->getFontFamily(), config->getFontSize()));
	painter.drawText(QRect(0,0,22,22), config->getLabel(), QTextOption(Qt::AlignLeft | Qt::AlignTop));

	setIcon(QIcon(pm));
}
