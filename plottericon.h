#ifndef PLOTTERICON_H
#define PLOTTERICON_H

#include <QSystemTrayIcon>

class PlotterConfig;

class PlotterIcon : public QSystemTrayIcon
{
public:
	PlotterIcon(PlotterConfig *, QObject * parent = 0);
	void inputData(quint64);

	int getLogSize() const;
	void setLogSize(int value);

	void redraw();


protected:
	int logSize;
	quint64 * log;
	int cursor;
	PlotterConfig *config;
};

#endif // PLOTTERICON_H
