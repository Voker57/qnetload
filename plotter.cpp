#include "plotter.h"

#include <QSettings>
#include <QFile>
#include <QTimer>
#include <QMenu>
#include <cmath>

#include "plotterconfig.h"
#include "plotterconfigwidget.h"
#include "plottericon.h"

Plotter::Plotter(QObject *parent) :
	QObject(parent)
{
	config = new PlotterConfig;
	plotterConfigWidget = new PlotterConfigWidget(config);
	plotterIcon = new PlotterIcon(config, this);
	plotterIcon->show();
	firstData = true;
	prevData = 0;
	updateTimer = new QTimer(this);
	connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateData()));
}

Plotter::~Plotter()
{
	delete config;
}

void Plotter::readConfig(QSettings *st)
{
	config->readConfig(st);
	firstData = true;
	prevData = 0;
	updateTimer->start(config->getUpdateTime());
}

void Plotter::writeConfig(QSettings *st)
{
	config->writeConfig(st);
}

void Plotter::setMenu(QMenu *menu)
{
	plotterIcon->setContextMenu(menu);
}

void Plotter::updateData()
{
	QString direction;
	if(config->getRx()) direction = "rx"; else direction = "tx";
	QString dataPath = QString("/sys/class/net/%1/statistics/%2_bytes").arg(config->getInterface()).arg(direction);
	QFile file(dataPath);
	if(!file.open(QFile::ReadOnly))
	{
		qDebug("Opening %s failed: %s", dataPath.toUtf8().data(), file.errorString().toUtf8().data());
	}
	quint64 data = file.readAll().trimmed().toULongLong();
	if(firstData)
	{
		firstData = false;
	} else
	{
		plotterIcon->inputData(data - prevData);
		plotterIcon->setToolTip(QString("%1 %2: %3 kb/s").arg(config->getInterface()).arg(config->getDirection()).arg((quint64)round((double)(data - prevData) / (double)1024 * (double)(config->getUpdateTime() / (double)1000))));
	}
	prevData = data;
	plotterIcon->redraw();
	file.close();
}
