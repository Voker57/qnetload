#include "plotterconfigwidget.h"

#include "plotterconfig.h"

#include <QWidget>
#include <QComboBox>
#include <QTextEdit>
#include <QFontComboBox>
#include <QGridLayout>
#include <QLabel>
#include <QDir>
#include <QPushButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QSpacerItem>
#include <QColorDialog>
#include <QSettings>
#include <QDebug>
#include <cmath>

PlotterConfigWidget::PlotterConfigWidget(PlotterConfig * pC, QWidget *parent) :
	QWidget(parent)
{
	config = pC;
	interfaceCombo = new QComboBox(this);
	rxTxCombo = new QComboBox(this);
	colorChooser = new QPushButton(this);
	backgroundColorChooser = new QPushButton(this);
	gridColorChooser = new QPushButton(this);
	textColorChooser = new QPushButton(this);
	scaleEdit = new QLineEdit(this);
	labelEdit = new QLineEdit(this);
	fontFamilyCombo = new QFontComboBox(this);
	fontSizeCombo = new QComboBox(this);
	gridDensitySpin = new QSpinBox(this);
	updateTimeEdit = new QLineEdit(this);

	connect(fontFamilyCombo, SIGNAL(currentFontChanged(QFont)), this, SLOT(resetFontSizes(QFont)));
	connect(colorChooser, SIGNAL(pressed()), this, SLOT(chooseColor()));
	connect(textColorChooser, SIGNAL(pressed()), this, SLOT(chooseTextColor()));
	connect(backgroundColorChooser, SIGNAL(pressed()), this, SLOT(chooseBackgroundColor()));
	connect(gridColorChooser, SIGNAL(pressed()), this, SLOT(chooseGridColor()));

	QGridLayout *gLayout = new QGridLayout(this);
	gLayout->setAlignment(gLayout, Qt::AlignLeft | Qt::AlignTop);
	QDir dir("/sys/class/net");
	interfaceCombo->addItems(dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs));
	QLabel *label = new QLabel("Interface: ", this);
	label->setBuddy(interfaceCombo);
	gLayout->addWidget(label, 0, 0);
	gLayout->addWidget(interfaceCombo, 0, 1);
	connect(interfaceCombo, SIGNAL(currentIndexChanged(int)), this, SIGNAL(titleChanged()));

	label = new QLabel("Direction: ", this);
	label->setBuddy(interfaceCombo);
	gLayout->addWidget(label, 0, 2);
	rxTxCombo->addItem("tx");
	rxTxCombo->addItem("rx");
	gLayout->addWidget(rxTxCombo, 0, 3);
	connect(rxTxCombo, SIGNAL(currentIndexChanged(int)), this, SIGNAL(titleChanged()));

	label = new QLabel("Graph color: ", this);
	label->setBuddy(colorChooser);
	gLayout->addWidget(label, 1, 0);
	gLayout->addWidget(colorChooser, 1, 1);

	label = new QLabel("Background color: ", this);
	label->setBuddy(backgroundColorChooser);
	gLayout->addWidget(label, 1, 2);
	gLayout->addWidget(backgroundColorChooser, 1, 3);

	label = new QLabel("Grid color: ", this);
	label->setBuddy(gridColorChooser);
	gLayout->addWidget(label, 2, 0);
	gLayout->addWidget(gridColorChooser, 2, 1);

	label = new QLabel("Spacing: ", this);
	label->setBuddy(gridDensitySpin);
	gLayout->addWidget(label, 2, 2);
	gLayout->addWidget(gridDensitySpin, 2, 3);

	label = new QLabel("Scale: ", this);
	label->setBuddy(scaleEdit);
	gLayout->addWidget(label, 3, 0);
	gLayout->addWidget(scaleEdit, 3, 1);
	label = new QLabel("kbytes/s", this);
	label->setBuddy(scaleEdit);
	gLayout->addWidget(label, 3, 2);

	label = new QLabel("Update interval: ", this);
	label->setBuddy(updateTimeEdit);
	gLayout->addWidget(label, 3, 3);
	gLayout->addWidget(updateTimeEdit, 3, 4);
	label = new QLabel("ms", this);
	label->setBuddy(updateTimeEdit);
	gLayout->addWidget(label, 3, 5);

	label = new QLabel("Label: ", this);
	label->setBuddy(labelEdit);
	gLayout->addWidget(label, 4, 0);
	gLayout->addWidget(labelEdit, 4, 1);
	connect(labelEdit, SIGNAL(textChanged(QString)), this, SIGNAL(titleChanged()));
	label = new QLabel("Font: ", this);
	label->setBuddy(fontFamilyCombo);
	gLayout->addWidget(label, 4, 2);
	gLayout->addWidget(fontFamilyCombo, 4, 3);
	label = new QLabel("Size: ", this);
	label->setBuddy(fontSizeCombo);
	gLayout->addWidget(label, 4, 4);
	gLayout->addWidget(fontSizeCombo, 4, 5);
	label = new QLabel("Color: ", this);
	label->setBuddy(textColorChooser);
	gLayout->addWidget(label, 4, 6);
	gLayout->addWidget(textColorChooser, 4, 7);

	gLayout->setRowStretch(5,1);
	gLayout->setColumnStretch(8,1);

	setLayout(gLayout);
}

QString PlotterConfigWidget::getTitle()
{
	return QString("%1 %2 (%3)").arg(interfaceCombo->currentText()).arg(rxTxCombo->currentText()).arg(labelEdit->text());
}


void PlotterConfigWidget::chooseColor()
{
	config->setColor(QColorDialog::getColor(config->getColor(), this));
	reloadConfig();
}

void PlotterConfigWidget::chooseBackgroundColor()
{
	config->setBackgroundColor(QColorDialog::getColor(config->getBackgroundColor(), this));
	reloadConfig();
}

void PlotterConfigWidget::chooseGridColor()
{
	config->setGridColor(QColorDialog::getColor(config->getGridColor(), this));
	reloadConfig();
}

void PlotterConfigWidget::chooseTextColor()
{
	config->setTextColor(QColorDialog::getColor(config->getTextColor(), this));
	reloadConfig();
}

void PlotterConfigWidget::resetFontSizes(QFont fnt)
{
	fontSizeCombo->clear();
	QStringList fontSizes;
	int ps;
	QFontDatabase db;
	foreach(ps, db.pointSizes(fnt.family()))
	{
		fontSizes.append(QString::number(ps));
	}
	fontSizeCombo->addItems(fontSizes);
}

void PlotterConfigWidget::reloadConfig()
{
	textColorChooser->setStyleSheet(QString("QPushButton {background:%1}").arg(config->getTextColor().name()));
	colorChooser->setStyleSheet(QString("QPushButton {background:%1}").arg(config->getColor().name()));
	gridColorChooser->setStyleSheet(QString("QPushButton {background:%1}").arg(config->getGridColor().name()));
	backgroundColorChooser->setStyleSheet(QString("QPushButton {background:%1}").arg(config->getBackgroundColor().name()));

	interfaceCombo->setCurrentIndex(interfaceCombo->findText(config->getInterface()));

	QString direction;
	if(config->getRx()) direction = "rx"; else direction = "tx";
	rxTxCombo->setCurrentIndex(rxTxCombo->findText(direction));

	scaleEdit->setText(QString::number(round((double)config->getScale() / 1024)));

	labelEdit->setText(config->getLabel());

	fontFamilyCombo->setCurrentFont(QFont(config->getFontFamily()));

	fontSizeCombo->setCurrentIndex(fontSizeCombo->findText(QString::number(config->getFontSize())));

	gridDensitySpin->setValue(config->getGridDensity());

	updateTimeEdit->setText(QString::number(config->getUpdateTime()));

	emit titleChanged();
}

void PlotterConfigWidget::writeConfig()
{
	config->setInterface(interfaceCombo->currentText());
	if(rxTxCombo->currentText() == "rx")
		config->setRx(true);
	else
		config->setRx(false);
	config->setScale(scaleEdit->text().toULongLong() * 1024);
	config->setLabel(labelEdit->text());
	config->setFontFamily(fontFamilyCombo->currentFont().family());
	config->setFontSize(fontSizeCombo->currentText().toInt());
	config->setGridDensity(gridDensitySpin->value());
	config->setUpdateTime(updateTimeEdit->text().toInt());
}
