isEmpty(PREFIX) {
	PREFIX = /usr/local
}

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qnetload
TEMPLATE = app
CONFIG += release

SOURCES += main.cpp\
    qnetload.cpp \
    plottericon.cpp \
    plotterconfigwidget.cpp \
    plotterconfig.cpp \
    plotter.cpp \
    plottersettings.cpp

HEADERS  += \
    qnetload.h \
    plottericon.h \
    plotterconfigwidget.h \
    plotterconfig.h \
    plotter.h \
    plottersettings.h

target.path = $$PREFIX/bin/
INSTALLS += target
