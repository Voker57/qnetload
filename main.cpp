#include "qnetload.h"

#include <QApplication>
#include <QSystemTrayIcon>
#include <QDebug>

int main(int argc, char *argv[])
{
	QNetLoad a(argc, argv);
	return a.exec();
}
