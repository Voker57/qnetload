#include "qnetload.h"
#include "plottericon.h"
#include "plotterconfig.h"
#include "plotter.h"
#include "plottersettings.h"

#include <QPixmap>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QFile>
#include <QDebug>
#include <QPainter>
#include <QSettings>
#include <QDir>
#include <QMenu>
#include <QLabel>

QNetLoad::QNetLoad(int & argc, char ** argv) :
	QApplication(argc, argv)
{
	setOrganizationDomain("bitcheese.net");
	setOrganizationName("qnetload");
	setApplicationName("qnetload");
	setQuitOnLastWindowClosed(false);

	aboutLabel = new QLabel("qnetload, advanced network monitor for system tray<br />"
	"inspired by knetload<br />"
	"more info: <a href=\"http://bitcheese.net/wiki/qnetload\">http://bitcheese.net/wiki/qnetload</a>");
	aboutLabel->setAlignment(Qt::AlignHCenter);

	plotterSettings = new PlotterSettings();

	menu = new QMenu("qnetload");
	QAction *action;
	action = new QAction("&Configure qnetload...", menu);
	connect(action, SIGNAL(triggered()), plotterSettings, SLOT(show()));
	menu->addAction(action);
	action = new QAction("&About qnetload...", menu);
	connect(action, SIGNAL(triggered()), aboutLabel, SLOT(show()));
	menu->addAction(action);
	menu->addSeparator();
	action = new QAction("E&xit", menu);
	connect(action, SIGNAL(triggered()), this, SLOT(quit()));
	menu->addAction(action);

	QSettings s(this);
	int size = s.beginReadArray("plotters");
	s.endArray();
	if(size == 0)
	{
		// Insert user-friendly defaults
		writeDefaultConfig();
	}
	readConfig();
	if(size == 0)
	{
		plotterSettings->show();
	}
	connect(plotterSettings, SIGNAL(configChanged()), this, SLOT(readConfig()));
}

void QNetLoad::writeDefaultConfig()
{
	QSettings st;
	QDir dir("/sys/class/net");
	QString ifacename;
	foreach(QString entry, dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs))
	{
		if(entry != "lo")
		{
			ifacename = entry;
			break;
		}
	}
	st.beginWriteArray("plotters", 2);
	st.setArrayIndex(0);
	st.setValue("interface", ifacename);
	st.setValue("rx", true);
	st.setValue("backgroundColor", QColor(0,0,0).name());
	st.setValue("gridColor", QColor(25,25,25).name());
	st.setValue("color", QColor(255,0,0).name());
	st.setValue("textColor", QColor(255,255,255).name());
	st.setValue("fontFamily", "DejaVu Sans");
	st.setValue("fontSize", "7");
	st.setValue("scale", 524288);
	st.setValue("label", "IN");
	st.setValue("gridDensity", 5);
	st.setValue("updateTime", 1000);
	st.setArrayIndex(1);
	st.setValue("interface", ifacename);
	st.setValue("rx", false);
	st.setValue("backgroundColor", QColor(0,0,0).name());
	st.setValue("gridColor", QColor(25,25,25).name());
	st.setValue("color", QColor(0,255,0).name());
	st.setValue("textColor", QColor(255,255,255).name());
	st.setValue("fontFamily", "DejaVu Sans");
	st.setValue("fontSize", "7");
	st.setValue("scale", 524288);
	st.setValue("label", "OUT");
	st.setValue("gridDensity", 5);
	st.setValue("updateTime", 1000);
	st.endArray();
}

void QNetLoad::close()
{
	exit(0);
}

void QNetLoad::readConfig()
{
	foreach(Plotter * pl, plotters)
	{
		delete pl;
	}
	plotters.clear();

	QSettings s;
	int size = s.beginReadArray("plotters");
	for(int i = 0; i < size; ++i)
	{
		s.setArrayIndex(i);
		Plotter *plotter = new Plotter(this);
		plotter->readConfig(&s);
		plotter->setMenu(menu);
		plotters << plotter;
	}
	s.endArray();
	plotterSettings->readConfig();
}
