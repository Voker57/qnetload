#ifndef QNETLOAD_H
#define QNETLOAD_H

#include "plotterconfig.h"

#include <QApplication>
#include <QList>

class QSystemTrayIcon;
class QTimer;
class PlotterIcon;
class Plotter;
class PlotterSettings;
class QMenu;
class QLabel;

class QNetLoad : public QApplication
{
	Q_OBJECT
public:
	QNetLoad(int & argc, char ** argv);
	void writeDefaultConfig();
signals:

public slots:
	void close();
	void readConfig();
protected:
	QList<Plotter *> plotters;
	PlotterSettings * plotterSettings;
	QMenu *menu;
	QLabel * aboutLabel;
};

#endif // QNETLOAD_H
