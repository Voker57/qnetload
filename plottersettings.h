#ifndef PLOTTERSETTINGS_H
#define PLOTTERSETTINGS_H
#include "plotter.h"

#include <QWidget>
#include <QList>

#include "plotterconfig.h"

class PlotterConfigWidget;
class QVBoxLayout;
class QTabWidget;
class QHBoxLayout;
class QPushButton;

class PlotterSettings : public QWidget
{
	Q_OBJECT
public:
	PlotterSettings(QWidget *parent = 0);
signals:
	void configChanged();
public slots:
	void destroyTab(int);
	void addTab();
	void readConfig();
	void writeConfig();
	void ok();
	void apply();
	void cancel();
	void updateTitles();
protected:
	QList<PlotterConfig*> plotterConfigs;
	QList<PlotterConfigWidget *> plotterConfigWidgets;
	QVBoxLayout *outerLayout;
	QHBoxLayout *buttonsLayout;
	QTabWidget *tabs;
	QPushButton *okButton;
	QPushButton *cancelButton;
	QPushButton *applyButton;
	QPushButton *addTabButton;
};

#endif // PLOTTERSETTINGS_H
