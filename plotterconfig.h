#ifndef PLOTTERCONFIG_H
#define PLOTTERCONFIG_H

#include <QString>
#include <QColor>
#include <QObject>

class QSettings;

class PlotterConfig : public QObject
{
	Q_OBJECT
public:
	PlotterConfig();
	QColor getColor() const;
	QColor getBackgroundColor() const;
	QColor getGridColor() const;
	QColor getTextColor() const;
	QString getInterface() const;
	bool getRx() const;
	quint64 getScale() const;
	QString getLabel() const;
	QString getFontFamily() const;
	int getFontSize() const;
	int getGridDensity() const;
	void readConfig(QSettings *);
	void writeConfig(QSettings *);
	int getUpdateTime() const;
	void setUpdateTime(int value);
	QString getDirection();

public slots:
	void setColor(const QColor &value);
	void setBackgroundColor(const QColor &value);
	void setGridColor(const QColor &value);
	void setTextColor(const QColor &value);
	void setInterface(const QString &value);
	void setRx(bool value);
	void setScale(const quint64 &value);
	void setLabel(const QString &value);
	void setFontFamily(const QString &value);
	void setFontSize(int value);
	void setGridDensity(int value);

protected:
	QColor color;
	QColor backgroundColor;
	QColor gridColor;
	QColor textColor;
	QString interface;
	bool rx;
	quint64 scale;
	QString label;
	QString fontFamily;
	int fontSize;
	int gridDensity;
	int updateTime;
};

#endif // PLOTTERCONFIG_H
