#ifndef PLOTTERCONFIGWIDGET_H
#define PLOTTERCONFIGWIDGET_H

#include <QWidget>
class QComboBox;
class QColorChooser;
class QLineEdit;
class QFontComboBox;
class QSpinBox;
class QPushButton;
class QSettings;
class PlotterConfig;

class PlotterConfigWidget : public QWidget
{
	Q_OBJECT
public:
	PlotterConfigWidget(PlotterConfig *, QWidget *parent = 0);
	QString getTitle();

protected:
	QComboBox *interfaceCombo;
	QComboBox *rxTxCombo;
	QPushButton *colorChooser;
	QPushButton *backgroundColorChooser;
	QPushButton *gridColorChooser;
	QPushButton *textColorChooser;
	QLineEdit *scaleEdit;
	QLineEdit *labelEdit;
	QFontComboBox *fontFamilyCombo;
	QComboBox *fontSizeCombo;
	QSpinBox *gridDensitySpin;
	QLineEdit *updateTimeEdit;
	PlotterConfig * config;

public slots:
	void chooseColor();
	void chooseBackgroundColor();
	void chooseGridColor();
	void chooseTextColor();
	void resetFontSizes(QFont);
	void reloadConfig();
	void writeConfig();

signals:
	void titleChanged();
};

#endif // PLOTTERCONFIGWIDGET_H
