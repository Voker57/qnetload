#ifndef PLOTTER_H
#define PLOTTER_H

#include <QObject>

class QSettings;
class PlotterIcon;
class PlotterConfig;
class PlotterConfigWidget;
class QTimer;
class QMenu;

class Plotter : public QObject
{
	Q_OBJECT
public:
	Plotter(QObject *parent = 0);
	~Plotter();
	void readConfig(QSettings *);
	void writeConfig(QSettings *);
	void setMenu(QMenu *);
signals:

public slots:
	void updateData();
protected:
	PlotterIcon * plotterIcon;
	PlotterConfig * config;
	PlotterConfigWidget * plotterConfigWidget;
	QTimer *updateTimer;
	quint64 prevData;
	bool firstData;
};

#endif // PLOTTER_H
