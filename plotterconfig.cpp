#include "plotterconfig.h"

#include <QSettings>
#include <QDebug>

PlotterConfig::PlotterConfig()
{

}

int PlotterConfig::getGridDensity() const
{
	if(gridDensity > 0)
		return gridDensity;
	else
		return 1;
}

void PlotterConfig::readConfig(QSettings *st)
{
	setInterface(st->value("interface").toString());
	setColor(QColor(st->value("color").toString()));
	setBackgroundColor(QColor(st->value("backgroundColor").toString()));
	setGridColor(QColor(st->value("gridColor").toString()));
	setTextColor(QColor(st->value("textColor").toString()));
	setRx(st->value("rx").toBool());
	setScale(st->value("scale").toULongLong());
	setLabel(st->value("label").toString());
	setFontFamily(st->value("fontFamily").toString());
	setFontSize(st->value("fontSize").toInt());
	setGridDensity(st->value("gridDensity").toInt());
	setUpdateTime(st->value("updateTime").toInt());
}

void PlotterConfig::writeConfig(QSettings *st)
{
	st->setValue("interface", getInterface());
	st->setValue("color", getColor().name());
	st->setValue("backgroundColor", getBackgroundColor().name());
	st->setValue("gridColor", getGridColor().name());
	st->setValue("textColor", getTextColor().name());
	st->setValue("rx", getRx());
	st->setValue("scale", getScale());
	st->setValue("label", getLabel());
	st->setValue("fontFamily", getFontFamily());
	st->setValue("fontSize", getFontSize());
	st->setValue("gridDensity", getGridDensity());
	st->setValue("updateTime", getUpdateTime());
}

void PlotterConfig::setGridDensity(int value)
{
	if(value > 0)
		gridDensity = value;
	else
		gridDensity = 0;
}

int PlotterConfig::getFontSize() const
{
	return fontSize;
}

void PlotterConfig::setFontSize(int value)
{
	fontSize = value;
}

QString PlotterConfig::getFontFamily() const
{
	return fontFamily;
}

void PlotterConfig::setFontFamily(const QString &value)
{
	fontFamily = value;
}

QString PlotterConfig::getLabel() const
{
	return label;
}

void PlotterConfig::setLabel(const QString &value)
{
	label = value;
}

quint64 PlotterConfig::getScale() const
{
	if(scale > 0)
		return scale;
	else
		return 1;
}

void PlotterConfig::setScale(const quint64 &value)
{
	if(value > 0)
		scale = value;
	else
		scale = 0;
}

bool PlotterConfig::getRx() const
{
	return rx;
}

void PlotterConfig::setRx(bool value)
{
	rx = value;
}

QString PlotterConfig::getInterface() const
{
	return interface;
}

void PlotterConfig::setInterface(const QString &value)
{
	interface = value;
}

QColor PlotterConfig::getTextColor() const
{
	return textColor;
}

void PlotterConfig::setTextColor(const QColor &value)
{
	textColor = value;
}

QColor PlotterConfig::getGridColor() const
{
	return gridColor;
}

void PlotterConfig::setGridColor(const QColor &value)
{
	gridColor = value;
}

QColor PlotterConfig::getBackgroundColor() const
{
	return backgroundColor;
}

void PlotterConfig::setBackgroundColor(const QColor &value)
{
	backgroundColor = value;
}

QColor PlotterConfig::getColor() const
{
	return color;
}

void PlotterConfig::setColor(const QColor &value)
{
	color = value;
}

int PlotterConfig::getUpdateTime() const
{
	if(updateTime < 100)
		return 100;
	else
		return updateTime;
}

void PlotterConfig::setUpdateTime(int value)
{
	if(value < 100)
		updateTime = 100;
	else
		updateTime = value;
}

QString PlotterConfig::getDirection()
{
	if(getRx())
		return QString("rx");
	else
		return QString("tx");
}
