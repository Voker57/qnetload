#include "plottersettings.h"

#include "plotter.h"
#include "plotterconfigwidget.h"
#include "plotterconfig.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTabWidget>
#include <QPushButton>
#include <QSettings>
#include <QTabBar>

PlotterSettings::PlotterSettings(QWidget *parent) :
	QWidget(parent)
{
	outerLayout = new QVBoxLayout;
	buttonsLayout = new QHBoxLayout;
	tabs = new QTabWidget(this);
	tabs->setTabsClosable(true);
	connect(tabs, SIGNAL(tabCloseRequested(int)), this, SLOT(destroyTab(int)));
	okButton = new QPushButton("OK", this);
	connect(okButton, SIGNAL(pressed()), this, SLOT(ok()));
	cancelButton = new QPushButton("Cancel", this);
	connect(cancelButton, SIGNAL(pressed()), this, SLOT(cancel()));
	applyButton = new QPushButton("Apply", this);
	connect(applyButton, SIGNAL(pressed()), this, SLOT(apply()));
	addTabButton = new QPushButton("Add tab", this);
	connect(addTabButton, SIGNAL(pressed()), this, SLOT(addTab()));
	setLayout(outerLayout);
	outerLayout->addWidget(tabs);
	buttonsLayout->addWidget(addTabButton);
	buttonsLayout->addStretch();
	buttonsLayout->addWidget(okButton);
	buttonsLayout->addWidget(cancelButton);
	buttonsLayout->addWidget(applyButton);
	outerLayout->addLayout(buttonsLayout);
	setLayout(outerLayout);
}

void PlotterSettings::readConfig()
{
	QSettings s;
	int size = s.beginReadArray("plotters");

	foreach(PlotterConfig *pc, plotterConfigs)
	{
		delete pc;
	}

	plotterConfigs.clear();

	for(int i = 0; i < tabs->count(); i++)
	{
		tabs->removeTab(0);
	}
	foreach(PlotterConfigWidget * w, plotterConfigWidgets)
	{
		delete w;
	}

	plotterConfigWidgets.clear();

	for(int i = 0; i < size; ++i)
	{
		s.setArrayIndex(i);
		PlotterConfig *pc = new PlotterConfig;
		pc->readConfig(&s);
		plotterConfigs << pc;
		PlotterConfigWidget *pcw = new PlotterConfigWidget(pc);
		tabs->addTab(pcw, "");
		plotterConfigWidgets << pcw;
		connect(pcw, SIGNAL(titleChanged()), this, SLOT(updateTitles()));
		pcw->reloadConfig();
	}
	s.endArray();
	if(plotterConfigs.size() == 1)
	{
		tabs->setTabsClosable(false);
	} else
	{
		tabs->setTabsClosable(true);
	}
}

void PlotterSettings::writeConfig()
{
	QSettings s;
	int i = 0;
	s.beginWriteArray("plotters", plotterConfigs.size());
	foreach(PlotterConfigWidget *pcw, plotterConfigWidgets)
	{
		PlotterConfig *pc =	plotterConfigs.at(i);
		pcw->writeConfig();
		s.setArrayIndex(i);
		pc->writeConfig(&s);
		i++;
	}
	s.endArray();
}

void PlotterSettings::ok()
{
	writeConfig();
	emit configChanged();
	hide();
}

void PlotterSettings::apply()
{
	writeConfig();
	emit configChanged();
}

void PlotterSettings::cancel()
{
	readConfig();
	hide();
}

void PlotterSettings::updateTitles()
{
	for(int i = 0; i < tabs->count(); i++)
	{
		tabs->setTabText(i, plotterConfigWidgets.at(i)->getTitle());
	}
}

void PlotterSettings::destroyTab(int tabIndex)
{
	delete plotterConfigs.at(tabIndex);
	plotterConfigs.removeAt(tabIndex);
	tabs->removeTab(tabIndex);
	delete plotterConfigWidgets.at(tabIndex);
	plotterConfigWidgets.removeAt(tabIndex);
	if(plotterConfigs.size() == 1)
	{
		tabs->setTabsClosable(false);
	}
}

void PlotterSettings::addTab()
{
	PlotterConfig *pc = new PlotterConfig;
	plotterConfigs << pc;
	PlotterConfigWidget *pcw = new PlotterConfigWidget(pc);
	tabs->addTab(pcw,QIcon(),"");
	plotterConfigWidgets << pcw;
	tabs->setTabsClosable(true);
}
